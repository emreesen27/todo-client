import React, { useEffect, useState } from "react";
import './App.css';
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faSave, faTimes, faTrash } from "@fortawesome/free-solid-svg-icons";

const App = (props) => {

  const [todo, setTodo] = useState([]);
  const [newTodo, setNewTodo] = useState();
  const [load, setLoad] = useState(false);
  const [typeTodo, setTypeTodo] = useState(false);
  const [saveButton, setSaveButton] = useState(false);
  const [updateValue, setUpdateValue] = useState("");

  useEffect(() => {
    axios.get('/todos')
      .then((response) => {
        setTodo(response.data)
      });

  }, []);

  const todoChange = (event) => {
    if (event.target.value !== "") {
      setTypeTodo(false);
      setLoad(true);
      setNewTodo(event.target.value);
    } else if (event.target.value === "") {
      setLoad(false);
    }

  }

  const deleteTodo = (todos, event) => {
    axios.delete(`/todos/${todos.id}`)
      .then((response) => {
        const arr = [...todo];
        const todoIndex = todo.indexOf(todos);
        arr.splice(todoIndex, 1);
        setTodo(arr);
      })

  }
  const updateTodo = (event) => {
    if (event.target.value !== "") {
      setSaveButton(true)
    } else {
      setSaveButton(false)
    }
    setUpdateValue(event.target.value);
  }

  const handleClickUpdateValue = (todo) => {

    const body = {
      title: updateValue,
      completed: true
    }

    axios.put(`/todos/${todo.id}`, body)
      .then((response) => {

      })

  }

  const handleCheckCompleted = (todo) => {

    const body = {
      title: todo.title,
      completed: false,
    }

    axios.put(`/todos/${todo.id}`, body)
      .then((response) => {
        axios.get('/todos')
          .then((response) => {
            setTodo(response.data)
          });
      })

  }

  const handleCheckNotCompleted = (todo) => {

    const body = {
      title: todo.title,
      completed: true
    }

    axios.put(`/todos/${todo.id}`, body)
      .then((response) => {
        axios.get('/todos')
          .then((response) => {
            setTodo(response.data)
          });
      })

  }


  const addTodo = () => {

    if (load === false) {
      setTypeTodo(true)
    } else if (load === true) {
      setTypeTodo(false);

      const body = {
        title: newTodo,
      }

      axios.post('/todos', body)
        .then((response) => {
          todo.push(response.data);
          setTodo([...todo], response.data);

        })
    }
  }

  return (
    <div className="App" id="app">
      <h2>
        <span>TODO</span>
        <input autoFocus id="write-todo" placeholder="Write todo" onChange={todoChange}/>
        <button id="add-btn" onClick={addTodo}>+</button>
      </h2>
      {
        load &&
        <p style={{ color: "#a63be4" }}>✏️ Writing todo..</p>
      }
      {
        typeTodo &&
        <p style={{ color: "red" }}>❌ Writing todo..</p>
      }
      {todo.map((todo, index) => (
        (
          <div style={{ marginBottom: "15px", background: "#d8d3d3", padding: "10px", borderRadius: "10px" }}>
            <input className={todo.completed === false ? "changeInp" : "changeInpTrue"} onChange={updateTodo} defaultValue={todo.title} />
            <button id="delete-btn" onClick={(event, id) => deleteTodo(todo, event)} style={{ float: "right", width: "50px", height: "40px", background: "#e91e63", fontSize: "12px", marginTop: "6px" }}>  <FontAwesomeIcon style={{ fontSize: "1.5em" }} icon={faTrash} /></button>
            {
              saveButton &&
              <button id="save-button" onClick={(event, id) => handleClickUpdateValue(todo, event)} style={{ float: "right", width: "50px", height: "40px", background: "#388e3c", fontSize: "12px", marginTop: "6px" }}><FontAwesomeIcon style={{ fontSize: "1.5em" }} icon={faSave} /></button>
            }

            {
              todo.completed === false ?
                <button id="handle-not-check" onClick={(event, id) => handleCheckNotCompleted(todo, event)} style={{ float: "right", width: "50px", height: "40px", background: "#1e88e5", fontSize: "12px", marginTop: "6px", marginLeft: "-2px" }}><FontAwesomeIcon style={{ fontSize: "1.5em" }} icon={faTimes} /></button>
                :
                <button id="handle-check" onClick={(event, id) => handleCheckCompleted(todo, event)} style={{ float: "right", width: "50px", height: "40px", background: "#1e88e5", fontSize: "12px", marginTop: "6px", marginLeft: "-2px" }}><FontAwesomeIcon style={{ fontSize: "1.5em" }} icon={faCheck} /></button>
            }
          </div>
        )
      ))}
    </div>
  );
}

export default App;
