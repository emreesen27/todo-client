# Todo App Client (React)

## Run and Tests

### Run the project

install npm first
```
npm install
```
you can run it now
```
npm start
```

### Running tests

Run your unit tests
```
npm run cypress
```

### [Live Demo](http://159.89.20.6/)
