describe('Todo', () => {
    it('did be input focus ?', () => {
        cy.visit('http://159.89.20.6');

        cy.focused()
        .get('#write-todo')
        .should('have.focus');
    });

    context('Add Todo', () => {
        const todo = 'test-todo';
    
        it('Add Todo', () => {
            cy.get('#write-todo')
            .type(todo)
            .should('have.value', todo);
        });

        it('Has todo been added?', () => {
            cy.get('#add-btn')
            .click();
        });
    });

    context('Edit Todo', () => {
        const editTodo = 'denem 121212';

        it('Edit TODO', () => {        
            cy.get('.changeInp').eq(0)
            .click();

            cy.get('.changeInp').eq(0)
            .type(editTodo);

            cy.get('#save-button').eq(0)
            .click();
        });
    });

    context('todo deleted?', () => {
        it('Was the delete button clicked?', () => {
            cy.get('#delete-btn')
            .click();
        });
    });

});
